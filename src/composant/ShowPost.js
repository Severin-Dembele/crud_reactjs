import React from 'react'
import { useNavigate } from 'react-router-dom'
export default function ShowPost() {
    const redirect = useNavigate();
    return (

        <div>

            <div className="row">
                <div className="col-auto me-auto"><p className="fs-1">Liste des posts</p></div>
                <div className="col-auto">
                    <button type="button" className="btn btn-success" onClick={() => { redirect('/createpost') }}>Nouveau</button>
                </div>
            </div>
            <div className="row justify-content-center">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Handle</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td colspan="2">Larry the Bird</td>
                            <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

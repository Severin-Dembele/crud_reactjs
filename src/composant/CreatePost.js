import React from 'react'

export default function CreatePost() {
    return (
        <div className='container'>
            <div className='n-center'>
                <p className="fs-1">Creer un nouvel post</p>
            </div>
            <>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Titre</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                    <div id="emailHelp" className="form-text">Saisir un titre</div>
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label">Slug</label>
                    <input type="text" className="form-control" id="exampleInputPassword1" />
                </div>
                <div className="mb-3">
                    <label htmlFor="corps" className="form-label">Corps</label>
                    <input type="text" className="form-control" id="corps" />
                </div>
                <div className="mb-3">
                    <label for="published" className="form-label">Disabled select menu</label>
                    <select id="published" className="form-select">
                        <option>option 1</option>
                        <option>option 1</option>
                        <option>option 1</option>
                    </select>
                </div>

                <div className='n-center'>
                    <button type="submit" className="btn btn-success n-center">Enregistrer</button>
                </div>
            </>

        </div>
    )
}

import React from 'react';
import { Link } from 'react-router-dom';

export default function Navbar() {
    return (
        <div>
            <ul className="nav justify-content-end">
                <Link style={{ textDecoration: 'none', paddingTop: '15px', paddingRight:'20px' }} to={`post`}>
                    <li className="nav-item">
                        Post
                    </li>
                </Link>
                <Link style={{ textDecoration: 'none', paddingTop: '15px' , paddingRight:'20px'}} to={`commentaire`}>
                    <li className="nav-item">
                       Commetaires
                    </li>
                </Link>
                <Link style={{ textDecoration: 'none', paddingTop: '15px' , paddingRight:'150px'}} to={`user`}>
                    <li className="nav-item">
                        Utilisateurs
                    </li>
                </Link>
            </ul>
        </div>
    )
}

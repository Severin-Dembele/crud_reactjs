import { Route,Routes,Navigate } from "react-router-dom";
import {ShowPost,ShowComment,ShowUser,CreateComment,CreatePost,CreateUser} from './composant'
import './App.css'


import Navbar from "./Navbar";
function App() {
  return (
    <div className="App">
      <Navbar />
      <br/>
      <Routes>
        <Route path="/post" element={<ShowPost />} />
        <Route path="/user" element={<ShowUser />} />
        <Route path="/commentaire" element={<ShowComment />} />
        <Route path="/createpost" element={<CreatePost />} />
        <Route path="/createuser" element={<CreateUser />} />
        <Route path="/createcomment" element={<CreateComment />} />
        <Route path="/404" element={<h1>Erreur page non trouvé</h1>} />
        <Route path="/*" element={<Navigate to="/404" />} />
      </Routes>

    </div>
  );
}

export default App;
